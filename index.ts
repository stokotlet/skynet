import {myContainer} from './src/config/inversify.config'
import {IBot} from "./src/app/types";
import {TYPES} from "./src/types";

const bot = myContainer.get<IBot>(TYPES.IBot)
bot.init()
