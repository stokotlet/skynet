import {ICommand} from "./command.interface";
import {IBot} from "../app/types";
import {Telegraf} from "telegraf";
import {IBotContext} from "../context/context.interface";

class Start extends ICommand{
    constructor(public bot: Telegraf<IBotContext>) {
        super(bot)
    }

    handle() {
        this.bot.start((ctx) => {
            ctx.reply('Привяо!!!!!!!')
        })
    }
}

export default Start
