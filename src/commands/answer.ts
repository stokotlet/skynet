import {ICommand} from "./command.interface";
import {Telegraf} from "telegraf";
import {IBotContext} from "../context/context.interface";
import OpenAI from "openai";

export class Answer extends ICommand{
    gpt = new OpenAI({apiKey: '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'})
    constructor(bot: Telegraf<IBotContext>) {
        super(bot)
    }

    handle() {

        this.bot.on('text', async (ctx) => {
            // let req = {choices: [{message: {content: 'ошибочка'}}]};
            // try {
            //     // @ts-ignore
            //     req = await this.gpt.chat.completions.create({
            //         messages: [{role: "user", content: 'расскажи любой анекдот'}],
            //         model: "gpt-3.5-turbo"
            //     })
            // }
            // catch (e) {
            //     // @ts-ignore
            //     ctx.reply('ой песдес не работает')
            //     console.log(e)
            // }
            const req = await this.gpt.chat.completions.create({
                        messages: [{role: "user", content: 'расскажи любой анекдот'}],
                        model: "gpt-3.5-turbo"
                    })

            ctx.reply(req.choices[0].message.content!)

        })
    }
}
