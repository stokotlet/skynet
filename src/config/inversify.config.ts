import { Container } from "inversify";
import { TYPES} from "../types";
import { IBot } from "../app/types";
import {IConfig} from "./config.interface";
import Bot from '../app'
import ConfigService from "./config.service";
import "reflect-metadata";

const myContainer = new Container();
myContainer.bind<IBot>(TYPES.IBot).to(Bot);
myContainer.bind<IConfig>(TYPES.ConfigInterface).to(ConfigService);

export { myContainer };
