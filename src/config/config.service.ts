import {IConfig} from "./config.interface";
import {DotenvParseOutput, config} from 'dotenv'
import { injectable, inject } from "inversify";
import "reflect-metadata";

@injectable()
class ConfigService implements IConfig {
    private config: DotenvParseOutput;
    constructor() {
        const {parsed, error} = config()
        if(error || !parsed) {
            throw new Error()
        }
        this.config = parsed;
    }

    get(key: string): string {
        return this.config[key]
    }
}

export default ConfigService;
