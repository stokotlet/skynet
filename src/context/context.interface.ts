import {Context} from "telegraf";

export interface ISessionData {
    ololo: string
}

export interface IBotContext extends Context{
    session: ISessionData;
}
