import {IBot} from "./types";
import { injectable, inject } from "inversify";
import {TYPES} from "../types";
import {IConfig} from "../config/config.interface";
import "reflect-metadata";
import {session, Telegraf} from "telegraf";
import {IBotContext} from "../context/context.interface";
import {ICommand} from "../commands/command.interface";
import Start from "../commands/start";
import {Answer} from "../commands/answer";

@injectable()
class Bot implements IBot {
    private token: string
    public bot: Telegraf<IBotContext>
    public commands: ICommand[] = []
    public constructor(
        @inject(TYPES.ConfigInterface) config: IConfig,

    ) {
        this.token = config.get('TOKEN')
        this.bot = new Telegraf<IBotContext>(this.token)
        this.bot.use(session())
        this.commands = [new Start(this.bot), new Answer(this.bot)]
    }

    init() {
        this.bot.launch()
        this.commands.forEach(command => command.handle())
    }
}

export default Bot;
