const TYPES = {
    ConfigInterface: Symbol.for("ConfigInterface"),
    IBot: Symbol.for("IBot"),
};

export { TYPES };
